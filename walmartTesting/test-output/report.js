$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("file:src/test/resources/features/VerifyWalmartLogo.feature");
formatter.feature({
  "name": "verify Craigslist logo",
  "description": "As a user\nI want to be able to browse the landing page of Walmart\nSo that I can verify Walmart logo",
  "keyword": "Feature",
  "tags": [
    {
      "name": "@Smoke"
    },
    {
      "name": "@Regression"
    }
  ]
});
formatter.scenario({
  "name": "Verify logo in landing page of Walmart",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@Smoke"
    },
    {
      "name": "@Regression"
    }
  ]
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "I am in Chrome Browser",
  "keyword": "Given "
});
formatter.match({
  "location": "step_defs.VerifyWalmartLogo.i_am_in_Chrome_Browser()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I browse Walmart landing page",
  "keyword": "When "
});
formatter.match({
  "location": "step_defs.VerifyWalmartLogo.i_browse_Walmart_landing_page()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I will see the Walmart logo",
  "keyword": "Then "
});
formatter.match({
  "location": "step_defs.VerifyWalmartLogo.i_will_see_the_Walmart_logo()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I will see 12 type of item are visible",
  "keyword": "Then "
});
formatter.match({
  "location": "step_defs.VerifyWalmartLogo.i_will_see_type_of_item_are_visible(java.lang.Integer)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Verify List 12 Types are available",
  "keyword": "And "
});
formatter.match({
  "location": "step_defs.VerifyWalmartLogo.verify_List_Types_are_available(java.lang.Integer)"
});
formatter.result({
  "status": "passed"
});
formatter.after({
  "status": "passed"
});
});