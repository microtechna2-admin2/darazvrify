package runner;

import org.junit.runner.RunWith;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

@RunWith(Cucumber.class)
@CucumberOptions(
	features={"src\\test\\resources\\features"},
	glue={"step_defs"},
	dryRun=false,
	monochrome=true,
	strict=false,
	tags={"@Regression or @Smoke"},
	plugin = {"pretty", "html:test-output", "json:target/cucumber-report/cucumber.json"}
)


public class runner {

}
