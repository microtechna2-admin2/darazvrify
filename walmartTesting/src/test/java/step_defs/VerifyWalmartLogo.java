package step_defs;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.github.bonigarcia.wdm.WebDriverManager;

import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.Assert;

import Base.Base;
public class VerifyWalmartLogo extends Base {
  
	
           //span[contains(text(), 'Pickup & delivery')]
  
		@Given("I am in Chrome Browser")
		public void i_am_in_Chrome_Browser() throws InterruptedException {
		 
		  url("https://www.daraz.com.bd/#");
			 Assert.assertEquals(browserName(), "chrome");
			 Thread.sleep(3000);
		}

		@When("I browse Walmart landing page")
		public void i_browse_Walmart_landing_page() {
		 boolean cp=   isDisplayed(By.xpath("//a[contains(text(), 'SAVE MORE ON APP')]"));
		Assert.assertEquals(cp, true);
		
		}

		@Then("I will see the Walmart logo")
		public void i_will_see_the_Walmart_logo() {
		boolean cp = isDisplayed(By.xpath("//*[@id=\"topActionHeader\"]/div/div[2]/div/div[1]/a/img"));
		  Assert.assertEquals(cp, true); 
		}
           
		@Then("I will see {int} type of item are visible")
		public void i_will_see_type_of_item_are_visible(Integer int1) {
			List<WebElement> myElements;
			myElements = driver.findElements(By.xpath("//*[@id=\"J_3442298940\"]/div/ul/li"));
		  int actual_size = myElements.size();
		  Assert.assertEquals(actual_size, 12);
		
		
		}

		@Then("Verify List {int} Types are available")
		public void verify_List_Types_are_available(Integer int1) {
		    List<WebElement> myElements;
		    myElements = driver.findElements(By.xpath("//div[@class=\"lzd-site-nav-menu-dropdown\"]/ul/li"));
		
		    
		    Assert.assertEquals(myElements.get(0).getText(),"Electronic Devices");
		    Assert.assertEquals(myElements.get(1).getText(),"Electronic Accessories");
		    Assert.assertEquals(myElements.get(2).getText(),"TV & Home Appliances");
		    Assert.assertEquals(myElements.get(3).getText(),"Health & Beauty");
		    Assert.assertEquals(myElements.get(4).getText(),"Babies & Toys");
		    Assert.assertEquals(myElements.get(5).getText(),"Groceries & Pets");
		    Assert.assertEquals(myElements.get(6).getText(),"Home & Lifestyle");
		    Assert.assertEquals(myElements.get(7).getText(),"Women's Fashion");
		    Assert.assertEquals(myElements.get(8).getText(),"Men's Fashion");
		    Assert.assertEquals(myElements.get(9).getText(),"Watches & Accessories");
		    Assert.assertEquals(myElements.get(10).getText(),"Sports & Outdoor");
		    Assert.assertEquals(myElements.get(11).getText(),"Automotive & Motorbike");

		    
		       
		    
		    
//		    for (WebElement element: myElements) {	
//				System.out.println( element.getText() );
//			}
//			
		
		}

}//class
