package step_defs;



import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import Base.Base;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;
import io.github.bonigarcia.wdm.WebDriverManager;
import io.qameta.allure.Attachment;
public class Hook extends Base{
	
	@Before
	public void setup() {
		System.out.println("Cucumber Before");
		ChromeOptions cOption = new ChromeOptions();
		cOption.setHeadless(false);
//		
		WebDriverManager.chromedriver().setup();  
		driver = new ChromeDriver(cOption);  //here we use ChromeDriver to instantiate driver variable
		
	}
	
	@After
	public void tearDown(Scenario scenario) {
		try {
			String screenshotName= scenario.getName().replace("", "");
			if(scenario.isFailed()) {
				scenario.log("this is my failure message");
				TakesScreenshot ts = (TakesScreenshot)driver;
				byte[]screenshot = ts.getScreenshotAs(OutputType.BYTES);
				scenario.attach(screenshot, "image/png", screenshotName);
			}
//			allure-cucumber5-jvm - 2.13.6
//			allure-junit5 - 2.13.6
//			cucumber-java - 5.7.0
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		
		
		System.out.println("Cucumber After");
		driver.quit();
	}	
}
