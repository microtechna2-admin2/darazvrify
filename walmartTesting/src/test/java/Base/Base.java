package Base;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;



public class Base {
	public static WebDriver driver;// declare driver as WebDriver object - Class(Global) variable

	public static String browserName() {
		return ((RemoteWebDriver) driver).getCapabilities().getBrowserName();
	}

	public void url(String url) {
		driver.get(url);
	}

	public boolean isDisplayed(By locator) {
		return driver.findElement(locator).isDisplayed();
	}

	public void click(By locator) {
		// driver.findElement( locator ).click();
		// OR
		WebElement element; // declare a variable named element of WebElement Data Type
		element = driver.findElement(locator);
		element.click();
	}

	public String getCurrentUrl() {
		String url = driver.getCurrentUrl();
		return url;
	}

	public String getText(By locator) {
		return driver.findElement(locator).getText();
	}
}
